<?php
function protocol_form_system_theme_settings_alter(&$form, &$form_state) {
	$form['header_icons'] = array(
		'#type'          => 'textarea',
		'#title'         => t('Header Icons'),
		'#default_value' => theme_get_setting('header_icons'),
		'#description'   => t("Place html icons in the header spot on your site."),
	);
}