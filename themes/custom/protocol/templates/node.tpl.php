<div id="node-<?php print $node->nid; ?>" class="div-1xn-container <?php print $classes; ?> clearfix"<?php print $attributes; ?>>
  <div class="div-components">
    <?php if ($user_picture): ?>
      <div class="div-components-thumbnail"><?php print $user_picture; ?></div>
    <?php endif; ?>
    <div class="div-components-content">
      <div class="title"><a href="<?php print $node_url; ?>"><?php print $title; ?></a></div>
      <div class="body">
      <?php if ($display_submitted): ?>
        <div class="submitted">
          <?php print $submitted; ?>
        </div>
      <?php endif; ?>
      <?php 
        hide($content['comments']);
        hide($content['links']);
        print render($content);
        print render($content['links']);
      ?>
      </div>
    </div>
  </div>
  <?php print render($content['comments']); ?>
</div>