<div class="wrapper">
	<div class="header clearfix">
		<?php if(isset($logo)):?>
			<a href="<?php print $front_page;?>" class="logo"><img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" /></a>
		<?php endif;?>
		<?php if ($site_name): ?>
			<h1 class="sitename"><a href="<?php print $front_page;?>">
				<?php print $site_name; ?> 
				<?php if ($site_slogan): ?>
					<span class="slogan"><?php print $site_slogan; ?></span>
				<?php endif;?>
			</a></h1>
		<?php endif;?>
    <?php print theme_get_setting('header_icons');?>
		<?php if($user->uid > 0):?>
		<nav class="homemenu">
			<ul>
				<li class="divgreetuser">Welcome </li>
				<li class="divuser"><?php print $user->name;?></li>
			</ul>
		</nav>
		<?php endif;?>
	</div>
	<div class="main clearfix">
		<div class= "navmenu clearfix">
			<?php print render($page['sidebar']); ?>
		</div>
		<div class="searchcontainer clearfix">
			<?php if ($messages): ?>
				<div id="messages"><?php print $messages; ?></div>
			<?php endif; ?>
			<div class="form-container">
				<?php if ($breadcrumb): ?>
					<div id="breadcrumb"><?php print $breadcrumb; ?></div>
				<?php endif; ?>
				<a id="main-content"></a>
				<?php print render($title_prefix); ?>
				<?php if ($title): ?>
					<h2><?php print $title;?></h2>
				<?php endif; ?>
				<?php print render($title_suffix); ?>
				<div class="content_container">
					<?php if ($tabs): ?>
						<div class="tabs">
							<?php print render($tabs); ?>
						</div>
					<?php endif; ?>
					<?php if ($action_links): ?>
						<ul class="action-links">
							<?php print render($action_links); ?>
						</ul>
					<?php endif; ?>
					<?php print render($page['content']);?>
				</div>
				<div class="footer"><?php print render($page['footer']);?></div>
			</div>
		</div>
	</div>
</div>