<?php

/**
 * Override or insert variables into the maintenance page template.
 */
function protocol_admin_preprocess_maintenance_page(&$vars) {
  shiny_preprocess_html($vars);
}
