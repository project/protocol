api = 2
core = 7.x

projects[admin_menu][version] = "3.0-rc5"
projects[admin_menu][subdir] = "contrib"

projects[adminimal_admin_menu][version] = "1.7"
projects[adminimal_admin_menu][subdir] = "contrib"

projects[module_filter][version] = "2.0"
projects[module_filter][subdir] = "contrib"

projects[user_registrationpassword][version] = "1.4"
projects[user_registrationpassword][subdir] = "contrib"

projects[ctools][version] = "1.11"
projects[ctools][subdir] = "contrib"

projects[date][version] = "2.9"
projects[date][subdir] = "contrib"

projects[ds][version] = "2.14"
projects[ds][subdir] = "contrib"

projects[features][version] = "2.10"
projects[features][subdir] = "contrib"

projects[addressfield][version] = "1.3"
projects[addressfield][subdir] = "contrib"

projects[addressfield_phone][version] = "1.3"
projects[addressfield_phone][subdir] = "contrib"

projects[entityreference][version] = "1.2"
projects[entityreference][subdir] = "contrib"

projects[field_collection][version] = "1.0-beta11"
projects[field_collection][subdir] = "contrib"

projects[field_group][version] = "1.5"
projects[field_group][subdir] = "contrib"

projects[field_group_inline][version] = "1.0-beta5"
projects[field_group_inline][subdir] = "contrib"

projects[field_hidden][version] = "1.7"
projects[field_hidden][subdir] = "contrib"

projects[field_reference][version] = "1.0"
projects[field_reference][subdir] = "contrib"

projects[references][version] = "2.1"
projects[references][subdir] = "contrib"

projects[serial][version] = "1.7"
projects[serial][subdir] = "contrib"

projects[views_field][version] = "1.2"
projects[views_field][subdir] = "contrib"

projects[homebox][version] = "2.0-rc3"
projects[homebox][subdir] = "contrib"

projects[auto_nodequeue][version] = "2.1"
projects[auto_nodequeue][subdir] = "contrib"

projects[nodequeue][version] = "2.1"
projects[nodequeue][subdir] = "contrib"

projects[backup_migrate][version] = "3.1"
projects[backup_migrate][subdir] = "contrib"

projects[comment_notify][version] = "1.3"
projects[comment_notify][subdir] = "contrib"

projects[css_injector][version] = "1.10"
projects[css_injector][subdir] = "contrib"

projects[diff][version] = "3.2"
projects[diff][subdir] = "contrib"

projects[entity][version] = "1.8"
projects[entity][subdir] = "contrib"

projects[formblock][version] = "1.0-alpha1"
projects[formblock][subdir] = "contrib"

projects[libraries][version] = "2.3"
projects[libraries][subdir] = "contrib"

projects[menu_html][version] = "1.0"
projects[menu_html][subdir] = "contrib"

projects[modal_forms][version] = "1.2"
projects[modal_forms][subdir] = "contrib"

projects[node_save_redirect][version] = "1.3"
projects[node_save_redirect][subdir] = "contrib"

projects[pathauto][version] = "1.3"
projects[pathauto][subdir] = "contrib"

projects[strongarm][version] = "2.0"
projects[strongarm][subdir] = "contrib"

projects[token][version] = "1.6"
projects[token][subdir] = "contrib"

projects[unique_field][version] = "1.0-rc1"
projects[unique_field][subdir] = "contrib"

projects[panels][version] = "3.8"
projects[panels][subdir] = "contrib"

projects[panels_style_pack][version] = "1.x-dev"
projects[panels_style_pack][subdir] = "contrib"

projects[panels_style_collapsible][version] = "1.3"
projects[panels_style_collapsible][subdir] = "contrib"

projects[rules][version] = "2.9"
projects[rules][subdir] = "contrib"

projects[honeypot][version] = "1.22"
projects[honeypot][subdir] = "contrib"

projects[ckeditor][version] = "1.17"
projects[ckeditor][subdir] = "contrib"

projects[better_exposed_filters][version] = "3.2"
projects[better_exposed_filters][subdir] = "contrib"

projects[views][version] = "3.14"
projects[views][subdir] = "contrib"

projects[views_bulk_operations][version] = "3.3"
projects[views_bulk_operations][subdir] = "contrib"

projects[views_field_view][version] = "1.2"
projects[views_field_view][subdir] = "contrib"

projects[views_php][version] = "1.0-alpha3"
projects[views_php][subdir] = "contrib"

projects[options_element][version] = "1.12"
projects[options_element][subdir] = "contrib"

projects[features_actions_triggers][version] = "1.1"
projects[features_actions_triggers][subdir] = "contrib"

projects[context][version] = "3.7"
projects[context][subdir] = "contrib"

projects[webform][version] = "4.14"
projects[webform][subdir] = "contrib"

projects[uuid][version] = "1.0-beta1"
projects[uuid][subdir] = "contrib"

projects[uuid_features][version] = "1.0-alpha4"
projects[uuid_features][subdir] = "contrib"

projects[menu_item_visibility][version] = "1.0-beta2"
projects[menu_item_visibility][subdir] = "contrib"

projects[newsletter][version] = "1.0-beta10"
projects[newsletter][subdir] = "contrib"
projects[newsletter][patch][] = https://www.drupal.org/files/issues/newsletter-field-exception-taxonomy-creation-2340511-9.patch

projects[fences][version] = "1.2"
projects[fences][subdir] = "contrib"

projects[codefilter][version] = "1.2"
projects[codefilter][subdir] = "contrib"

projects[google_analytics][version] = "2.2"
projects[google_analytics][subdir] = "contrib"

projects[google_analytics_et][version] = "1.3"
projects[google_analytics_et][subdir] = "contrib"

projects[metatag][version] = "1.17"
projects[metatag][subdir] = "contrib"

projects[jquery_update][version] = "2.7"
projects[jquery_update][subdir] = "contrib"

projects[path_breadcrumbs][version] = "3.3"
projects[path_breadcrumbs][subdir] = "contrib"

projects[entityconnect][version] = "1.0-rc5"
projects[entityconnect][subdir] = "contrib"

projects[nodechanges][version] = "1.0-rc1"
projects[nodechanges][subdir] = "contrib"

; ++++++ Libraries ++++++

libraries[ckeditor][download][type] = get
libraries[ckeditor][download][url] = http://download.cksource.com/CKEditor/CKEditor/CKEditor%204.5.6/ckeditor_4.5.6_standard.zip
libraries[ckeditor][destination] = libraries

; ++++++ Themes ++++++

; shiny
projects[shiny][type] = theme
projects[shiny][version] = 1.7
projects[shiny][subdir] = contrib