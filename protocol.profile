<?php

/**
 * @file
 * Enables modules and site configuration for a protocol profile site installation.
 */

/**
 * Implements hook_form_FORM_ID_alter() for install_configure_form().
 */
function protocol_form_install_configure_form_alter(&$form, $form_state) {
  $form['site_information']['site_name']['#default_value'] = 'Protocol 7 Enterprise Profile';
}

/**
 * Implements hook_install_tasks_alter().
 */
function protocol_install_tasks_alter(&$tasks, $install_state) {
	$tasks['install_finished']['function'] = 'protocol_install_finished';
	$tasks['install_select_profile']['display'] = FALSE;
	_protocol_set_theme('protocol_admin');
}

/**
 * Force-set a theme at any point during the execution of the request.
 */
function _protocol_set_theme($target_theme) {
  if ($GLOBALS['theme'] != $target_theme) {
    unset($GLOBALS['theme']);
    drupal_static_reset();
    $GLOBALS['conf']['maintenance_theme'] = $target_theme;
    _drupal_maintenance_theme();
  }
}

/**
 * Custom installation task; perform final steps and redirect the user to the new site if there are no errors.
 */
function protocol_install_finished(&$install_state) {
	$item = array(
    'link_title' => 'Log in',
    'menu_name'  => 'user-menu',
    'customized' => 1,
    'link_path'  => 'user/login',
    'module'     => 'menu'
  );
  $mlid = menu_link_save($item);
  if (isset($mlid)) {
    $query = db_insert('menu_links_visibility_role');
    $query->fields(array('mlid', 'rid'));
    $query->values(array('mlid' => $mlid, 'rid' => user_role_load_by_name('anonymous user')->rid));
    $query->execute();
  }
  drupal_set_title(st('@drupal installation complete', array('@drupal' => drupal_install_profile_distribution_name())), PASS_THROUGH);
  $messages = drupal_set_message();
  variable_set('install_profile', drupal_get_profile());
  variable_set('install_task', 'done');
  drupal_flush_all_caches();
  db_update('system')
    ->fields(array('weight' => 1000))
    ->condition('type', 'module')
    ->condition('name', drupal_get_profile())
    ->execute();
  drupal_get_schema(NULL, TRUE);
  drupal_cron_run();
  if (isset($messages['error'])) {
    $output = '<p>' . (isset($messages['error']) ? st('Review the messages above before visiting <a href="@url">your new site</a>.', array('@url' => url(''))) : st('<a href="@url">Visit your new site</a>.', array('@url' => url('')))) . '</p>';
    return $output;
  }
  else {
    drupal_get_messages('status', TRUE);
    drupal_get_messages('completed', TRUE);
    drupal_get_messages('ok', TRUE);
    $output = st('<p>Installation was completed successfully. <a href="@url">Visit your new site</a></p>', array('@url' => url('')));
    return $output;
  }
}
